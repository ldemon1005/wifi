$("#create_info").validate({
    ignore: [],
    rules: {
        'info[name]': {
            required: true,
        },
        'info[caption]': {
            required: true,
        }
    },
    messages: {
        'info[name]': {
            required: 'Thiếu tên thông tin sản phẩm',
        },
        'info[caption]': {
            required: 'Thiếu mô tả thông tin sản phẩm',
        }
    }
});