<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{asset('local/resources/assets/lib/font.css')}}"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('local/resources/assets/lib/bootstrap.min.css')}}/">
	<script type="text/javascript" src="{{asset('local/resources/assets/lib/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('local/resources/assets/lib/popper.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('local/resources/assets/lib/bootstrap.min.js')}}"></script>
	<!--build:css css/styles.min.css-->
	<link rel="stylesheet" href="{{asset('local/resources/assets/css/styles.css')}}">
	<link rel="stylesheet" href="{{asset('local/resources/assets/css/print.css')}}">
	<!--endbuild-->
	@yield('css')
</head>
<body>
	<div id="wrapper">
		@include('client.header-footer.header')

		@yield('main')

		@include('client.header-footer.footer')
        <div class="errorAlert">
            @include('errors.note')
        </div>
	</div>

	<!--build:js js/main.min.js -->
	<script src="{{asset('local/resources/assets/js/lib/a-library.js')}}js/lib/a-library.js"></script>
	<script src="{{asset('local/resources/assets/js/lib/another-library.js')}}"></script>
	<script src="{{asset('local/resources/assets/js/main.js')}}"></script>
	<!-- endbuild -->


    <script>
        $('.errorAlert').css('bottom','100px');
        setTimeout(function(){
            $('.errorAlert').css('bottom', '-200px');
        }, 3000);
        setTimeout(function(){
            $('.errorAlert').fadeOut();
        }, 3900);
    </script>
	@yield('js')
</body>
</html>