<section class="advance position-relative">
    <div class="container">
        <div class="row box-advance">
            <div class="box-advance_text text-center c-secondary">
                <h4>Thuê ngay wifi du lịch cho chuyến đi của bạn</h4>
                <p>Trải nghiệm kết nối internet 4G tốc độ cao ở bất cứ đâu mà bạn đặt chân tới.</p>
            </div>
            <div class="box-advance_button">
                <a href="#">
                    <button class="form-control btn btn-default">Đặt thuê</button>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="footer position-relative">
    <div class="container">
        <div class="row box-footer c-secondary">
            <p>© 2018 Tadiwifi. All rights reserved.</p>
            <div class="box-footer_social">
                <ul class="list-social">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <p>Made by ABC. Designed by Hoang Vu.</p>
        </div>
    </div>
</section>