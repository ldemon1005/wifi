<section class="first-section home position-relative h-768">
    <header>
        <div class="header">
            <div class="container">
                <div class="row box-header">
                    <div class="box-header_logo_text col-md-4 col-sm-4 col-8 text-uppercase mg-at">Tadiwifi.vn</div>
                    <div class="box-header_menu col-md-8 mg-at text-right">
                        <ul class="list-menu">
                            <li class="active">
                                <a class="pointer" href="#">Trang chủ</a>
                            </li>
                            <li><a class="pointer" href="#">Bài viết</a></li>
                            <li><a class="pointer" href="contact.html">Liên hệ</a></li>
                            <li><a href="#">
                                    <button class="form-control btn-default box-header_menu_rent pointer">Đặt thuê</button>
                                </a></li>
                        </ul>
                    </div>
                    <div class="col-sm-8 col-4 menu-repons" style="display: none">
                        <div class="nav-toggle pointer" onclick="showMenu()">
                            <span class="fa fa-bars"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="function">
        <div class="container">
            <div class="row">
                <div class="box-function c-secondary mg-at">
                    <div class="box-function_text">
                        <h4>Thuê ngay wifi du lịch cho chuyến đi của bạn</h4>
                        <p>Trải nghiệm kết nối internet 4G tốc độ cao ở bất cứ đâu mà bạn đặt chân tới.</p>
                    </div>
                    <div class="box-function_content c-secondary">
                        <form method="post">
                            <div class="form-group row box-function_content_area">
                                <input type="text" name="area" placeholder="Bạn muốn đi đâu" class="form-control">
                            </div>
                            <div class="form-group row box-function_content_time">
                                <input type="text" name="time_start" placeholder="Ngày bắt đầu"
                                       class="form-control mg-r-2">
                                <input type="text" name="time_end" placeholder="Ngày kết thúc"
                                       class="form-control mg-l-5">
                            </div>
                            <button type="submit" class="form-control btn-default pointer">Đặt thuê</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>