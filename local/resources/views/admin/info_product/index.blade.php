@extends('admin.master')
@section('title', 'Quản trị')
@section('main')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 ">Danh sách thông tin sản phẩm</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ asset('admin') }}">Trang chủ</a></li>
                            <li class="breadcrumb-item active">Danh sách thông tin sản phẩm</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('form_info',0)}}" class="btn btn-primary">Thêm mới</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Tiêu đề</th>
                                    <th>Avatar</th>
                                    <th>Mô tả</th>
                                    <th>Ngày tạo</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($list_info as $info)
                                    <tr>
                                        <td>{{$info->name}}</td>
                                        <td>
                                            <div class="avatar">
                                                <img src="{{file_exists(storage_path('app/info/resized500-'.$info->avatar)) ? asset('local/storage/app/info/resized500-'.$info->avatar) : asset('local/resources/assets/images/default-image.png')}}">
                                            </div>
                                        </td>
                                        <td>{{$info->caption}}</td>
                                        <td>{{date('d/m/Y H:m',$info->created_at)}}</td>
                                        <td class="text-center">
                                            <button id="{{$info->id}}" onclick="update_status({{$info->id}})" class="btn btn-block btn-sm {{$info->status == 2 ? 'btn-success': 'btn-danger'}}">{{$info->status == 2 ? 'Hoạt đông': 'Không hoạt đông'}}</button>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{route('form_info',$info->id)}}" data-toggle="tooltip" title="Chỉnh sửa" class="col-sm-4 text-primary"><i class="fa fa-wrench"></i></a>

                                            <a href="{{route('delete_info',$info->id)}}" onclick="return confirm('Bạn chắc chắn muốn xóa')" data-toggle="tooltip" title="Xóa" class="col-sm-4 text-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row form-group pull-right" style="margin: 10px 0px">
                                {{$list_info->links()}}
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="detail_group" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    </div>
@stop

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
@stop

@section('script')
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <script>
        function update_status(id) {
            $.ajax({
                url: '/admin/info_product/update_status_info/'+id,
                method: 'get',
                dataType: 'json',
            }).fail(function (ui, status) {
            }).done(function (data, status) {
                if(data.info){
                    data.info = JSON.parse(data.info);

                    var id = '#' + data.info.id;

                    if(data.info.status == 2) {
                        $(id).removeClass('btn-danger');
                        $(id).addClass('btn-success');
                        $(id).html('Hoạt động');
                    }else {
                        $(id).removeClass('btn-success');
                        $(id).addClass('btn-danger');
                        $(id).html('Không hoạt động');
                    }
                }
            });
        }
    </script>
@stop