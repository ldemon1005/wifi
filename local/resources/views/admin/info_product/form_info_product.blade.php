@extends('admin.master')

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
@stop
@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ $info->id == 0? 'Thêm mới ': 'Chỉnh sửa '}}Thông tin sản phẩm</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ asset('admin') }}">Trang chủ</a></li>
                            <li class="breadcrumb-item"><a href="">Thông tin sản phẩm</a></li>
                            <li class="breadcrumb-item active">{{ $info->id == 0? 'Thêm mới ': 'Chỉnh sửa '}}</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12 col-sm-12">
                        <div class="card card-danger">
                            <div class="card-header">
                                <h3 class="card-title">{{ $info->id == 0? 'Thêm mới ': 'Chỉnh sửa '}}</h3>
                            </div>
                            <form id="create_info" action="{{route('action_info')}}" method="post"
                                  enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <input type="text" name="info[id]" value="{{$info->id}}"
                                           class="form-control d-none" placeholder="ID danh mục">

                                    <div class="row form-group">
                                        <label class="col-sm-2">Tên thông số <span
                                                    class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="info[name]" required
                                                   value="{{$info->name}}"
                                                   class="form-control" placeholder="Tên thông số">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-sm-2">Mô tả <span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <textarea type="text" name="info[caption]" class="form-control"
                                                      placeholder="Mô tả">{{$info->caption}}</textarea>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-sm-2">Ảnh đại diện</label>
                                        <div class="col-sm-3 form-group">
                                            <input id="img" type="file" name="img" class="cssInput"
                                                   onchange="changeImg(this)" style="display: none!important;">
                                            <img style="cursor: pointer;max-width: 100%;max-height: 300px;" id="avatar"
                                                 class="cssInput thumbnail imageForm"
                                                 src="{{file_exists(storage_path('app/info/resized500-'.$info->avatar)) ? asset('local/storage/app/info/resized500-'.$info->avatar) : asset('local/resources/assets/images/default-image.png')}}">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-sm-2">Trạng thái</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <label class="col-sm-3 text-primary">
                                                    <input value="2" type="radio"
                                                           name="info[status]" {{ $info->status == 2 ? 'checked' : '' }}>
                                                    Hoạt động
                                                </label>
                                                <label class="col-sm-3 text-primary">
                                                    <input value="1" type="radio"
                                                           name="info[status]" {{ $info->status != 2 ? 'checked' : '' }}>
                                                    Không hoạt động
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-info pull-right"
                                                style="margin-right: 10px">{{ $info->id ? 'Cập nhật' : 'Tạo mới' }}</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ./row -->
    </div>
@stop

@section('script')
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('editor1', {
                height: '400px',
                filebrowserBrowseUrl: 'plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: 'plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: 'plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: 'plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: 'plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: 'plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });
        });

    </script>

    <script>
        $("#create_info").validate({
            ignore: [],
            rules: {
                'info[title]': {
                    required: true
                },
                'info[caption]': {
                    required: true
                }
            },
            messages: {
                'info[title]': {
                    required: 'Vui lòng nhập tên danh mục'
                },
                'info[caption]': {
                    required: 'Vui lòng nhập mô tả tin tức'
                }
            }
        });
    </script>
@stop
