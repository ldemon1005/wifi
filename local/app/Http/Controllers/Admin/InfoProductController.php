<?php

namespace App\Http\Controllers\Admin;

use App\Models\InfoProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InfoProductController extends Controller
{
    function index(){
        $list_info = DB::table('info_product')->paginate(15);

        $data = [
            'list_info' => $list_info
        ];

        return view('admin.info_product.index',$data);
    }

    function form_info($id = 0){
        if($id == 0){
            $info = [
                'id' => 0,
                'name' => '',
                'avatar' => '',
                'status' => 2,
                'caption' => ''
            ];

            $info = (object)$info;
        }else {
            $info = InfoProduct::find($id);
        }

        $data = [
            'info' => $info
        ];

        return view('admin.info_product.form_info_product',$data);
    }

    function action_info(Request $request){
        $req = $request->get('info');

        $image = $request->file('img');
        if ($request->hasFile('img')) {
            $req['avatar'] = saveImageArticle([$image], 'info');
        }

        if($req['id'] == 0){
            $req['created_at'] = time();

            if($info = InfoProduct::create($req)){
                return redirect()->route('list_info')->with('success','Tạo mới thành công');
            }else {
                return redirect()->route('form_info',0)->with('error','Tạo mới không thành công');
            }
        }else {
            $info = InfoProduct::find($req['id']);

            if($info->update($req)){
                return redirect()->route('list_info')->with('success','Cập nhật thành công');
            }else {
                return redirect()->route('form_info',0)->with('error','Cập nhật không thành công');
            }
        }
    }

    function update_status($id){
        $info = InfoProduct::find($id);
        $info->status == 2 ? $info->status = 1 : $info->status = 2;
        $info->save();

        return json_encode([
            'info' => $info->toJson()
        ]);
    }

    function delete_info($id){
        if(DB::table('info_product')->delete($id)){
            return redirect()->route('list_info')->with('success','Xóa thành công');
        }else {
            return redirect()->route('list_info')->with('error','Xóa không thành công');
        }
    }
}
