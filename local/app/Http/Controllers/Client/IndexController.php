<?php

namespace App\Http\Controllers\Client;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{
    function index(){
        $list_article = DB::table('article')->where('status',2)->orderByDesc('id')->take(3)->get();

        $list_info = DB::table('info_product')->where('status',2)->orderByDesc('id')->get();

        $data = [
            'list_article' => $list_article,
            'list_info' => $list_info
        ];
        return view('client.index.home',$data);
    }

    function action_question(Request $request){
        $req = $request->all();

        $req['status'] = 1;

        $req['created_at'] = time();

        if(Question::create($req)){
            return redirect()->route('home')->with('success','Gửi câu hỏi thành công');
        }else {
            return redirect()->route('home')->with('error','Gửi câu hỏi không thành công');
        }
    }
}
