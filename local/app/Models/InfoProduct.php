<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoProduct extends Model
{
    protected $table = 'info_product';

    public $timestamps = false;

    public $guarded = [];
}
