<?php
use \Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin','middleware' => 'CheckLoginAdmin','prefix' => 'admin'],function (){
    Route::get('','IndexController@index')->name('admin');
    Route::group(['prefix' => 'account'],function (){
        Route::get('','AccountController@index')->name('list_user');
        Route::get('/update_status_account/{id}','AccountController@update_status')->name('update_status_account');
    });

    Route::group(['prefix' => 'question'],function (){
        Route::get('','QuestionController@index')->name('list_question');
        Route::get('/update_status_question/{id}','QuestionController@update_status')->name('update_status_question');
        Route::get('/form_send_mail/{id}','QuestionController@form_send_mail')->name('form_send_mail');
        Route::post('/send_email','QuestionController@send_email')->name('send_mail');
    });

    Route::group(['prefix' => 'article'],function (){
        Route::get('','ArticleController@index')->name('list_article');
        Route::get('/update_status_article/{id}','ArticleController@update_status')->name('update_status_article');
        Route::get('/delete_article/{id}','ArticleController@delete_article')->name('delete_article');
        Route::get('form_article/{id}','ArticleController@form_article')->name('form_article');
        Route::post('action_article','ArticleController@action_article')->name('action_article');
    });

    Route::group(['prefix' => 'info_product'],function (){
        Route::get('','InfoProductController@index')->name('list_info');
        Route::get('/update_status_info/{id}','InfoProductController@update_status')->name('update_status_info');
        Route::get('/delete_info/{id}','InfoProductController@delete_info')->name('delete_info');
        Route::get('form_info/{id}','InfoProductController@form_info')->name('form_info');
        Route::post('action_info','InfoProductController@action_info')->name('action_info');
    });
});

Route::group(['namespace' => 'Client'],function (){
    Route::get('','IndexController@index')->name('home');

    Route::post('action_question','IndexController@action_question')->name('action_question');

    Route::get('login_client','AuthController@form_login')->name('login_client');
    Route::post('post_login_client','AuthController@login')->name('post_login_client');
    Route::get('register_client','AuthController@form_register')->name('register_client');
    Route::post('post_register_client','AuthController@register')->name('post_register_client');

    Route::get('logout_client','AuthController@logout')->name('logout_client');
});



Route::get('login', 'Admin\LoginController@getLogin')->middleware('CheckLogoutAdmin');
// Route::post('login', 'Admin\LoginController@postLogin');

Route::post('login', [ 'as' => 'login', 'uses' => 'Admin\LoginController@postLogin']);

Route::get('logout', 'Admin\LoginController@getLogout');
Route::get('lockscreen', 'Admin\LoginController@getLockScreen');
Route::post('lockscreen', 'Admin\LoginController@postLockScreen');
